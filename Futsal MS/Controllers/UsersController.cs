﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Futsal_MS.DAL;
using Futsal_MS.Models;

namespace Futsal_MS.Controllers
{
    public class UsersController : Controller
    {
        private FutsalMSContext db = new FutsalMSContext();

        // GET: Users
        public ActionResult Index()
        {
            return View(db.Persons.ToList());
        }

        // GET: Users/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Person person)
        {

           // if (person.Position == null)
         //   {
     
                //person.Position = Position.User;
          //  }///
            if (ModelState.IsValid)
            {
                if(db.Persons.Any(u => u.Email == person.Email))
                {
                    TempData["error"] = "Email already exists";
                    return RedirectToAction("Create");
                }
                else
                {
                    TempData["success"] = "Your account has been created.";
                    db.Persons.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            return View(person);
            
        }

        // GET: Users/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "P_ID,Firstname,Lastname,Email,Password,ContactNo,Gender")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.Persons.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.Persons.Find(id);
            db.Persons.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Users/Create
        public ActionResult Login()
        {
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login([Bind(Include = "P_ID,Firstname,Lastname,Email,Password,ContactNo,Gender")] Person person)
        {
            if (db.Persons.Any(u => u.Email == person.Email) && db.Persons.Any(u => u.Password == person.Password))
            {
               TempData["success"] = "You are logged in";
                Person persons = db.Persons.Where(x => x.Email == person.Email).FirstOrDefault();
                FormsAuthentication.SetAuthCookie(persons.P_ID.ToString(), false);
                Session["userId"] = persons.P_ID;
                Session["userName"] = persons.Firstname;
                return RedirectToAction("Index");
            }
            else
            {
                TempData["success"] = "You are not logged in.";
                ModelState.AddModelError("","Username and Password are Incorrect");
                return RedirectToAction("Login");
            }
            
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
