﻿using Futsal_MS.DAL;
using Futsal_MS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Futsal_MS.Controllers
{
    public class AdminController : Controller
    {
        private FutsalMSContext db = new FutsalMSContext();

        // GET: Admin
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AddUser(Person person)
        {
            if (ModelState.IsValid)
            {
                if (db.Persons.Any(u => u.Email == person.Email))
                {
                    TempData["error"] = "Email already exists";
                    return RedirectToAction("Create");
                }
                else
                {
                    TempData["success"] = "Your account has been created.";
                    db.Persons.Add(person);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                TempData["error"] = "It buypasses it";
                return View(person);
            }
        }
    }
}