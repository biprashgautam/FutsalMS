﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Futsal_MS.DAL;
using Futsal_MS.Models;
using System.Dynamic;
using Futsal_MS.Controllers;
using System.Globalization;

namespace Futsal_MS.Controllers
{
    [Authorize]
    public class BookingsController : Controller
    {
        private FutsalMSContext db = new FutsalMSContext();
        
        // GET: Bookings
        public ActionResult Index()
        {
            List<string> times = new List<string>();

            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("5:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");

            ViewBag.list = times;
            return View(db.Bookings.ToList());
        }
        
        // GET: Bookings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // GET: Bookings/Create
        /*public ActionResult Create()
        {
            return View();
        }*/
                
          public ActionResult Test()
        {
            List<string> times = new List<string>();
            
                 times.Add("7:00 AM");
                times.Add("8:00 AM");
                times.Add("9:00 AM");
                times.Add("10:00 AM");
                times.Add("11:00 AM");
                times.Add("12:00 PM");
                times.Add("1:00 PM");
                times.Add("2:00 PM");
                times.Add("3:00 PM");
                times.Add("4:00 PM");
                times.Add("6:00 PM");
                times.Add("7:00 PM");
                times.Add("8:00 PM");
            
            ViewBag.list = times;
            return View();
        }

        [HttpPost]
        public ActionResult Test(Booking bk)
        {
            return RedirectToAction("Test");
        }

       
        public ActionResult Create()
        {
            List<string> times = new List<string>();

            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("5:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");

            ViewBag.list = times;


           /* if (time != null)
            {
                Booking booking = new Booking();
                booking.time = time;
                booking.Price = 1000;
                booking.Person_id = Convert.ToInt32(Session["userId"]);
<<<<<<< HEAD
                //booking.Person.P_ID = (int)Session["userId"];
=======
                booking.Person.P_ID = (int)Session["userId"];
                TempData["success"] = "You have been booked.";
>>>>>>> e36df5a95ae211e818c5483d5a6e0983d81df291
                db.Bookings.Add(booking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            */
            /*List<Booking> book = new List<Booking>();
            List<FutsalPrice> fut = db.Futsalprices.ToList();
            DateTime d[];
            foreach (var item in fut)
            {
                d[].Add(item.Time);
            }
            Booking a = db.Bookings.Where(time in  d);*/
            //ViewBag.error = time?.AddDays(3);
            return View();
           
        }
         public List<Booking> GetBookings()
        {
            List<Booking> bk = db.Bookings.ToList();
            return bk;
        }

        [HttpDelete]
        [HttpPost]
        public ActionResult CancelBooking(DateTime? time)
        {
           

            if (time != null)
            {
                Booking booking = new Booking();
                booking.time = (DateTime)time;
                //booking.P_ID = (int)Session["userId"];
                booking.Name = (string)Session["userName"];
                db.Bookings.Remove(booking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            
            /*List<Booking> book = new List<Booking>();
            List<FutsalPrice> fut = db.Futsalprices.ToList();
            DateTime d[];
            foreach (var item in fut)
            {
                d[].Add(item.Time);
            }
            Booking a = db.Bookings.Where(time in  d);*/
            //ViewBag.error = time?.AddDays(3);
            return View(db.Futsalprices.ToList());

        }
        // POST: Bookings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create( Booking booking)
        {
            List<string> times = new List<string>();

            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("5:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");
            ViewBag.list = times;

            if (ModelState.IsValid)
            {
                if(booking.time<DateTime.Now.AddDays(-1))
                {
                    TempData["error"] = "Please choose valid date";
                    return RedirectToAction("Create");
                }
                else if(booking.time > DateTime.Now.AddDays(15))
                {
                    TempData["error"] = "You are being futuristic !!! 15 days from today available";
                    return RedirectToAction("Create");
                }
               if(db.Bookings.Any(u=> u.time==booking.time && u.times==booking.times))
                {
                    TempData["error"] = "The timing is already booked .";
                    return RedirectToAction("Create");

                }

              if(booking.time.DayOfWeek.ToString()=="Saturday")
                {
                    booking.Price = 2000;
                }
              else
                {
                    booking.Price = 1000;
                }
               // booking.P_ID = (int)Session["userId"];
                booking.Name =(string) Session["userName"];
                
                
               
               
               
               
                db.Bookings.Add(booking);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(booking);
        }

        // GET: Bookings/Edit/5
        public ActionResult Edit(int? id)
        {
            List<string> times = new List<string>();

            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("5:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");
            ViewBag.list = times;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // POST: Bookings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Booking booking)
        {
            List<string> times = new List<string>();

            times.Add("7:00 AM");
            times.Add("8:00 AM");
            times.Add("9:00 AM");
            times.Add("10:00 AM");
            times.Add("11:00 AM");
            times.Add("12:00 PM");
            times.Add("1:00 PM");
            times.Add("2:00 PM");
            times.Add("3:00 PM");
            times.Add("4:00 PM");
            times.Add("6:00 PM");
            times.Add("7:00 PM");
            times.Add("8:00 PM");
            ViewBag.list = times;
            if (ModelState.IsValid)
            {
                
                

                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(booking);
        }

        // GET: Bookings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Booking booking = db.Bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }
            return View(booking);
        }

        // POST: Bookings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            Booking booking = db.Bookings.Find(id);
            

            //comparing the time
            if()
            {
                TempData["error"] = "Unauthorize act";
                return RedirectToAction("Index");
            }
            string one = booking.time.ToShortDateString();
            DateTime dt1 = DateTime.ParseExact(one + " " + booking.times, "dd/MM/yy HH:mm:ss tt",
    CultureInfo.InvariantCulture);
            TimeSpan d1 = dt1.Subtract(DateTime.Now);
            if (Convert.ToInt32(d1.ToString("hh"))<1)
            {
                TempData["error"] = "you can't cancel booking before an hour";
                return RedirectToAction("Index");
            }
            db.Bookings.Remove(booking);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        
        public ActionResult DeleteBookingConfirmed(DateTime? time)
        {
            if (time == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Booking booking = db.Bookings.Where(x => x.time== time).FirstOrDefault();
            if(booking != null)
            {
                db.Bookings.Remove(booking);
                db.SaveChanges();
            }
            {
                // Display message that the booking does not exist.
            }
            return RedirectToAction("Create");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
