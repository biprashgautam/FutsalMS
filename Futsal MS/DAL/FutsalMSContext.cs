﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Futsal_MS.Models;

namespace Futsal_MS.DAL
{
    public class FutsalMSContext :DbContext
    {
        public FutsalMSContext() : base("FutsalDb")
        {

        }

        public DbSet<Person> Persons { get; set; }

        public DbSet<Booking> Bookings { get; set; }

        public DbSet<Membership> Memberships { get; set; }

        public DbSet<Package> Packages { get; set; }

        public DbSet<FutsalPrice> Futsalprices { get; set; }

        public DbSet<Feedback> Feedbacks { get; set; }

        public DbSet<BlockedUser> BlockedUsers { get; set; }

       

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}