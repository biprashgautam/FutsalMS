namespace Futsal_MS.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialss : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BlockedUser",
                c => new
                    {
                        Block_ID = c.Int(nullable: false, identity: true),
                        U_ID = c.Int(nullable: false),
                        Strike = c.Int(nullable: false),
                        Discription = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Block_ID);
            
            CreateTable(
                "dbo.Booking",
                c => new
                    {
                        Booking_ID = c.Int(nullable: false, identity: true),
                        time = c.DateTime(nullable: false),
                        times = c.String(),
                        Price = c.Int(nullable: false),
                        Name = c.String(),
                        person_P_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Booking_ID)
                .ForeignKey("dbo.Person", t => t.person_P_ID)
                .Index(t => t.person_P_ID);
            
            CreateTable(
                "dbo.Person",
                c => new
                    {
                        P_ID = c.Int(nullable: false, identity: true),
                        Firstname = c.String(nullable: false),
                        Lastname = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Password = c.String(nullable: false, maxLength: 20),
                        ContactNo = c.Long(nullable: false),
                        Position = c.Int(),
                    })
                .PrimaryKey(t => t.P_ID);
            
            CreateTable(
                "dbo.Feedback",
                c => new
                    {
                        F_ID = c.Int(nullable: false, identity: true),
                        Feedback_text = c.String(nullable: false),
                        U_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.F_ID);
            
            CreateTable(
                "dbo.FutsalPrice",
                c => new
                    {
                        Price_ID = c.Int(nullable: false, identity: true),
                        Time = c.String(nullable: false),
                        Price = c.Int(nullable: false),
                        ISweekend = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Price_ID);
            
            CreateTable(
                "dbo.Membership",
                c => new
                    {
                        M_ID = c.Int(nullable: false, identity: true),
                        Package_ID = c.Int(nullable: false),
                        U_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.M_ID);
            
            CreateTable(
                "dbo.Package",
                c => new
                    {
                        Package_ID = c.Int(nullable: false, identity: true),
                        Package_name = c.String(nullable: false),
                        Package_Duration = c.Int(nullable: false),
                        Package_Price = c.Int(nullable: false),
                        Package_Time = c.DateTime(nullable: false),
                        M_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Package_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Booking", "person_P_ID", "dbo.Person");
            DropIndex("dbo.Booking", new[] { "person_P_ID" });
            DropTable("dbo.Package");
            DropTable("dbo.Membership");
            DropTable("dbo.FutsalPrice");
            DropTable("dbo.Feedback");
            DropTable("dbo.Person");
            DropTable("dbo.Booking");
            DropTable("dbo.BlockedUser");
        }
    }
}
