﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Futsal_MS.Models
{
    public class BlockedUser
    {
        [Key]
        public int Block_ID { get; set; }

        public int U_ID { get; set; }

        [Required]
        public int Strike { get; set; }

        [Required]
        public string Discription { get; set; }
    }
}