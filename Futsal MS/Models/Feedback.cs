﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Futsal_MS.Models
{
    public class Feedback
    {
        [Key]
        public int F_ID { get; set; }

        [Required]
        public string Feedback_text { get; set; }

        public int U_ID { get; set; }
    }
}