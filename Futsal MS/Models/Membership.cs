﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Futsal_MS.Models
{
    public class Membership
    {
        [Key]
        public int M_ID { get; set; }

        public int Package_ID { get; set; }

        public int U_ID { get; set; }
    }
}