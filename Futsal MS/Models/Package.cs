﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Futsal_MS.Models
{
    public class Package
    {
        [Key]
        public int Package_ID { get; set; }

        [Required]
        public string Package_name { get; set; }

        [Required]
        public int Package_Duration { get; set; }

        [Required]
        public int Package_Price { get; set; }

        [Required]
        public DateTime Package_Time { get; set; }

        public int M_ID { get; set; }
    }
}