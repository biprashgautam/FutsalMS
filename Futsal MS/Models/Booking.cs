﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Futsal_MS.Models
{
    public class Booking
    {
        [Key]
        public int Booking_ID { get; set; }

        public DateTime time { get; set; }

        public string times { get; set; }

       // [NotMapped]
       // public int t { get; set; }
        public int Price { get; set; }
        
        public string Name { get; set; }
        public virtual Person person { get; set; }      
    }
}