﻿    using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Futsal_MS.Models
{
    public class FutsalPrice
    {
        [Key]
        public int Price_ID { get; set; }

        [Required(ErrorMessage = "This field cannot be empty")]
        public string Time { get; set; }

        [Required(ErrorMessage = "This fileld cannot be Empty.")]
        public int Price { get; set; }

        public bool ISweekend { get; set; }
    }
}